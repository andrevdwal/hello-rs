# Hello RS

This is a very basic "tutorial" app that:

 - adds a project dependency (clap)
 - sets up and parses command line args (cli.yml + clap)
 - uses those parameters to modify a `Hello` string and print it
