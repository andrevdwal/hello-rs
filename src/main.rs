#[macro_use]
extern crate clap;

fn main() {

    let yaml = load_yaml!("cli.yml");
    let matches = clap::App::from_yaml(yaml).get_matches();

    let reverse: bool = matches.is_present("reverse");
    let case: &str = matches.value_of("case").unwrap_or("default");

    let mut hello = String::from("Hello");
    if case == "lower" {
        hello = hello.to_lowercase();
    } else if case == "upper" {
        hello = hello.to_uppercase();
    }

    if reverse {
        hello = String::from(hello).chars().rev().collect::<String>();
    }

    println!("{}", hello);
}
